import numpy as np

# lattice vectors
a_cv = np.array([[  2.55265548,   0.        ,   0.        ],
                 [  1.27632774,   2.21066449,   0.        ],
                 [  0.        ,   0.        ,  55.25811025]])

# reciprocal lattice vectors
b_cv = 2 * np.pi * np.linalg.inv(a_cv).T

def get_momentum_transfer(q_c, a_cv=a_cv, b_cv=b_cv):
    q_v = np.dot(q_c, b_cv)
    return np.linalg.norm(q_v)


if __name__ == '__main__':
    for k in [81, 101]:
        q = get_momentum_transfer((1./float(k), 0, 0))
        print('k={0}: {1:.5f} Ang^-1'.format(k, q))

