import matplotlib.pyplot as plt
textwidth_points = 455.24408
textwidth = 6.299  # in
rcparams = {'font.size' : 14,
                        #'axes.labelsize' : 12,
                        'legend.fontsize': 14,
                        #'xtick.labelsize' : 12,
                        #'ytick.labelsize' : 12,
                        'font.family' : 'lmodern',
                        'text.latex.unicode': True,
                        #'lines.markersize': 11, 
                        #'backend': 'ps',
                        'text.usetex': True
                        }
plt.rcParams.update(rcparams)


