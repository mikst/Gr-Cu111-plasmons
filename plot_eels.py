import numpy as np
import matplotlib.pyplot as plt
from momentum import get_momentum_transfer


with open('cfg.py') as f:
    exec(f.read())


for q in range(1, 20):
    data1 = np.loadtxt('data/Cu111_a3.61_e400_k101_vac10.0_n100_nl6.eels%i.omega2.0_domega0.01_e100.csv' % q, delimiter=',')
    data2 = np.loadtxt('data/GCu111_a3.61_e400_k101_vac10.0_n100_nl6.eels%i.omega2.0_domega0.01_e100.csv' % q, delimiter=',')
    plt.figure(figsize=(4., 2.5))
    qv = get_momentum_transfer((q/101., 0, 0))
    plt.plot(data1[:, 0], data1[:, 2], 'b', label=r'Cu $|$ q=%.2f $\AA^{-1}$' % qv)
    plt.plot(data2[:, 0], data2[:, 2], 'r', label=r'G/Cu $|$ q=%.2f $\AA^{-1}$'% qv)
    plt.xlim(0, 1)
    plt.ylim(0, 0.03)
    plt.yticks(np.arange(0.0, 0.031, 0.01))
    plt.xlabel(r' frequency (eV)')
    plt.ylabel(r'loss function $S_M$')
    plt.legend(loc=2)
    plt.savefig('figures/MLG_Cu_Sm_q%i.svg' % q, bbox_inches='tight')
