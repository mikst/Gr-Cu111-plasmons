from ase.build import fcc111, add_adsorbate


def get_graphene(a=None, vacuum=None):
    slab = fcc111('X', size=(1, 1, 1), a=a, vacuum=vacuum)
    add_adsorbate(slab, 'C', 0.0, 'ontop')
    add_adsorbate(slab, 'C', 0.0, 'fcc')
    del slab[0]
    return slab


def get_slab(symbol, size, a=None, height=None, vacuum=None,
             add_graphene=True):
    if add_graphene:
        h = vacuum + height
    else:
        h = vacuum
    slab = fcc111(symbol, size=size, a=a, vacuum=h)
    if add_graphene:
        add_adsorbate(slab, 'C', height, 'ontop')
        add_adsorbate(slab, 'C', height, 'fcc')

    return slab

if __name__ == '__main__':
    from ase.visualize import view
    atoms = get_graphene(3.61, 10.0)
    atoms.set_pbc(True)
    view(atoms)
