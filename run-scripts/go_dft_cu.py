from gpaw import GPAW, PW, Mixer, Davidson
from make_structures import get_slab

ecut = 400
k = 81
nlayers = 14
vacuum = 15.0
a = 3.61
nbands = 400
name = 'Cu111_a{0}'.format(a) + '_e{0}'.format(ecut) + '_k{0}'.format(k) + \
        '_vac{0}'.format(vacuum) + '_n{0}'.format(nbands) + \
        '_nl{0}'.format(nlayers)

atoms = get_slab(symbol='Cu',
                 size=(1, 1, nlayers),
                 a=a,
                 vacuum=vacuum,
                 add_graphene=False)

atoms.set_pbc(True)
calc = GPAW(mode=PW(ecut),
            basis='dzp',
            mixer=Mixer(0.05, 5, 100.0),
            kpts=(20, 20, 1),
            eigensolver=Davidson(3),
            txt=name + '.txt')
atoms.set_calculator(calc)
atoms.get_potential_energy()
print 'ef scf:', calc.get_fermi_level()
calc.set(fixdensity=True,
         kpts=(k, k, 1),
         convergence={'bands': nbands},
         nbands=450)
atoms.get_potential_energy()
print 'ef nscf:', calc.get_fermi_level()
#calc.diagonalize_full_hamiltonian(nbands=nbands)
calc.write(name + '.gpw', 'all')
