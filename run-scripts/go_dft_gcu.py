from gpaw import GPAW, PW, Mixer, Davidson
from make_structures import get_slab

ecut = 400
k = 81
nlayers = 10
height = 3.25
vacuum = 15.0
a = 3.61
nbands = 200

name = 'GCu111_a{0}'.format(a) + \
        '_e{0}'.format(ecut) + \
        '_k{0}'.format(k) + \
        '_vac{0}'.format(vacuum) + \
        '_n{0}'.format(nbands) + \
        '_nl{0}'.format(nlayers)


atoms = get_slab(symbol='Cu',
                 size=(1, 1, nlayers),
                 a=a,
                 height=3.25,
                 vacuum=vacuum,
                 add_graphene=True)

atoms.set_pbc(True)

calc = GPAW(mode=PW(ecut),
            basis='dzp',
            #eigensolver=Davidson(3),
            mixer=Mixer(0.04, 5, 100.0),
            kpts=(41, 41, 1),
            txt=name + '.txt')
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.set(fixdensity=True,
         kpts=(k, k, 1),
         nbands=nbands+50,
         convergence={'bands': nbands, 'eigenstates': 1.0e-10})
atoms.get_potential_energy()
#calc.diagonalize_full_hamiltonian(nbands=nbands)
calc.write(name + '.gpw', 'all')
