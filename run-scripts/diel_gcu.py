from ase.build import bulk
from gpaw import GPAW, PW
from math import pi
from gpaw.response.df import DielectricFunction
k = 81
a = 3.61
vac = 15.0
ecut_in = 400
ecut = 100
nbands = 200
nlayers = 10
domega0 = 0.01
eta = 0.02
omega2 = 2.0
truncation = '2D'
name = 'GCu111_a{0}'.format(a) + '_e{0}'.format(ecut_in) +\
        '_k{0}'.format(k) + '_vac{0}'.format(vac) + '_n{0}'.format(nbands) + \
        '_nl{0}'.format(nlayers)

df = DielectricFunction(calc=name + '.gpw',
                        ecut=ecut,
                        nbands=nbands,
                        domega0=domega0,
                        omega2=omega2,
                        eta=eta,
                        truncation=truncation)

quit()
for i in range(1, 20):
    q_c = [float(i) / k, 0, 0]
    #print i, q_c[0] * 2 * pi / 2.552655 #
    filename = name + \
               '.eels{0}.'.format(i) + \
               'omega{0}'.format(omega2) + \
               '_domega{0}'.format(domega0) + \
               '_e{0}'.format(ecut) + \
               '_t{0}.csv'.format(truncation)
    #print filename
    df.get_eels_spectrum(q_c=q_c, filename=filename)

