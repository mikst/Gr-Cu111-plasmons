import numpy as np
from ase.parallel import paropen
from ase.io import read
from ase.build import bulk
from gpaw import GPAW, PW
from math import pi
from gpaw.response.df import DielectricFunction
k = 81
a = 3.61
vac = 15.0
ecut_in = 400
ecut = 100
nbands = 200
nlayers = 10
domega0 = 0.01
eta = 0.02
omega2 = 2.0
truncation = '2D'
name = 'Cu111_a{0}'.format(a) + \
        '_e{0}'.format(ecut_in) +\
        '_k{0}'.format(k) + \
        '_vac{0}'.format(vac) + \
        '_n{0}'.format(nbands) + \
        '_nl{0}'.format(nlayers)
df = DielectricFunction(calc=name + '.gpw',
                        ecut=ecut,
                        nbands=nbands,
                        domega0=domega0,
                        omega2=omega2,
                        eta=eta,
                        truncation=truncation)

atoms = read(name + '.gpw')
a_cv = atoms.get_cell()
b_cv = 2 * np.pi * np.linalg.inv(a_cv).T
fd = paropen(name+'.q_v.csv', 'w')
print >>fd, '#|q|, q_x, q_y, qz'
for i in range(1, 20):
    # momentum vector
    q_c = [float(i) / k, 0, 0]
    q_v = np.dot(b_cv.T, q_c)
    print >>fd, '{0}'.format(np.sqrt(np.dot(q_v, q_v))) +\
            ', {0}'.format(q_v[0]) +\
            ', {0}'.format(q_v[1]) +\
            ', {0}'.format(q_v[2])
    filename = name + \
               '.eels{0}.'.format(i) + \
               'omega{0}'.format(omega2) + \
               '_domega{0}'.format(domega0) + \
               '_e{0}'.format(ecut) + \
               '_t{0}.csv'.format(truncation)
    print filename
    df.get_eels_spectrum(q_c=q_c, filename=filename)

fd.close()
